Install [orange](https://orangedatamining.com/), in a python venv if you're feeling fancy:
```
pip install PyQt5 PyQtWebEngine
pip install orange3
```

Start Orange with our shared project:
```
orange-canvas all-wikis.ows
```
